import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Page from '@/components/Page'
import Post from '@/components/Post'
import List from '@/components/List'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Hello
    },
    {
      path: '/page/:id',
      component: Page
    },
    {
      path: '/post/:id',
      component: Post
    },
    {
      path: '/blog/',
      component: List
    }
  ],
  mode: 'history'
})
